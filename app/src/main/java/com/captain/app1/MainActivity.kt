package com.captain.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.captain.app1.databinding.ActivityMainBinding
import kotlin.math.log

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        findViewById<Button>(R.id.button).setOnClickListener{
            var intent = Intent(this,HelloActivity::class.java)
            var name = binding.textName.text.toString()
            Log.d("text_Name", name)
            intent.putExtra("name",name)
            this.startActivity(intent)

        }
    }
}